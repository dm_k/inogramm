//
//  Session.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Session.h"

static NSString *SessionTokenUserDefaultsKey = @"SessionTokenUserDefaultsKey";

@interface Session ()

@property (nonatomic, strong) NSUserDefaults *userDefaults;

@end

@implementation Session

- (instancetype)init {
    self = [super init];
    if (self) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
        _token = [_userDefaults objectForKey:SessionTokenUserDefaultsKey];
    }
    return self;
}

- (void)setToken:(NSString *)token {
    if (![_token isEqualToString:token]) {
        _token = token;
        [self.userDefaults setObject:token forKey:SessionTokenUserDefaultsKey];
        [self.userDefaults synchronize];
    }
}

@end
