//
//  InstagrammService.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *InstagrammServiceReachibilityChangedNotification;

typedef void (^InstagrammServiceCompletion) (BOOL success, NSError *error);
typedef void (^InstagrammServiceDataCompletion) (BOOL success, NSData *data, NSError *error);

@interface InstagrammService : NSObject

+ (instancetype)sharedService;

- (BOOL)online;

- (void)loginWithToken:(NSString *)token;

- (BOOL)isLoggedIn;

- (void)logoutWithCompletion:(InstagrammServiceCompletion)completion;

- (void)loadMediaWithCompletion:(InstagrammServiceDataCompletion)completion;

- (void)loadCommentsForMediaId:(NSString *)mediaId withCompletion:(InstagrammServiceDataCompletion)completion;

- (void)deleteComment:(NSString *)commentId fromMediaId:(NSString *)mediaId withCompletion:(InstagrammServiceCompletion)completion;

- (void)createCommentWithText:(NSString *)text forMediaId:(NSString *)mediaId withCompletion:(InstagrammServiceDataCompletion)completion;

- (void)performRequestWithURLString:(NSString *)URLString completion:(InstagrammServiceDataCompletion)completion;
- (void)performRequestWithURL:(NSURL *)URL completion:(InstagrammServiceDataCompletion)completion;
- (void)performRequest:(NSURLRequest *)request withCompletion:(InstagrammServiceDataCompletion)completion;

@end
