//
//  Pagination.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Pagination.h"

@implementation Pagination

+ (NSDictionary *)mapping {
    return @{
             @"next_url" : @"nextURL",
             @"next_max_id" : @"nextMaxId",
             };
}

- (void)setNextMaxId:(NSString *)nextMaxId {
    NSArray *idParts = [nextMaxId componentsSeparatedByString:@"_"];
    NSAssert(idParts.count == 2, @"Wrong id pattern");
    
    self.nextPhotoId = @([idParts[0] integerValue]);
    _nextMaxId = nextMaxId;
}

@end
