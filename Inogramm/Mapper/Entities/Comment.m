//
//  Comment.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 07/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "Comment.h"

@implementation Comment

+ (NSDictionary *)mapping {
    return @{
             @"id" :             @"commentId",
             @"created_time" :   @"date",
             @"text" :           @"text",
             @"from.id" :        @"fromId",
             @"from.full_name" : @"fromFullName",
            };
}

+ (NSString *)primaryKey {
    return @"commentId";
}

@end
