//
//  UIImage+Crop.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 12/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Addition)

+ (UIImage *)imageFromView:(UIView *)view drawedInRect:(CGRect)rect afterScreenUpdates:(BOOL)afterScreenUpdates;

- (UIImage *)cropFromRect:(CGRect)fromRect;

@end
