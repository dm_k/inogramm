//
//  CommentsModel.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 16/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Photo;
@class Comment;

@interface CommentsModel : NSObject

@property (nonatomic, strong, readonly) Photo *photo;

- (instancetype)initWithPhoto:(Photo *)photo;

- (void)reloadDataWithCompletion:(void (^)())completion;

- (void)deleteItemForRow:(NSInteger)row withCompletion:(void(^)(BOOL deleted))completion;

- (void)createItemWithText:(NSString *)text withCompletion:(void(^)(BOOL created))completion;

- (NSInteger)numberOfRows;
- (Comment *)itemForRow:(NSInteger)row;

@end
