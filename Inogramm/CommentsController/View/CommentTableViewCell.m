//
//  CommentTableViewCell.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 19/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "Comment.h"

@interface CommentTableViewCell ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@property (nonatomic, strong) UILabel *commentLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *fromLabel;

@end

@implementation CommentTableViewCell

+ (CGFloat)heightForItem:(Comment *)item widht:(CGFloat)widht {
    CGFloat actualWidht = widht - 8.0f * 2.0f;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    paragraphStyle.firstLineHeadIndent = 8.0f;
    paragraphStyle.alignment = NSTextAlignmentJustified;
    CGSize commentSize = [item.text boundingRectWithSize:CGSizeMake(actualWidht, CGFLOAT_MAX)
                                                 options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                              attributes:attributes
                                                 context:nil].size;
    
    paragraphStyle.firstLineHeadIndent = 0.0f;
    paragraphStyle.alignment = NSTextAlignmentRight;
    CGSize fromSize = [item.fromFullName boundingRectWithSize:CGSizeMake(actualWidht, CGFLOAT_MAX)
                                                      options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                   attributes:attributes
                                                      context:nil].size;
    NSString *dateString = [[CommentTableViewCell dateFormatter] stringFromDate:item.date];
    CGSize dateSize = [dateString boundingRectWithSize:CGSizeMake(actualWidht, CGFLOAT_MAX)
                                               options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                            attributes:attributes
                                               context:nil].size;
    return 8.0f + commentSize.height + 8.0f + fromSize.height + 4.0f + dateSize.height + 8.0f;
}

+ (NSDateFormatter *)dateFormatter {
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-mm-dd HH:mm"];
    });
    return dateFormatter;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        self.backgroundView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        _contentInsets = UIEdgeInsetsMake(8.0f,
                                          8.0f,
                                          8.0f,
                                          8.0f);
        
        _commentLabel = [[UILabel alloc] init];
        _commentLabel.numberOfLines = 0;
        _commentLabel.font =  [UIFont systemFontOfSize:[UIFont systemFontSize]];
        _commentLabel.textAlignment = NSTextAlignmentJustified;
        [self.contentView addSubview:_commentLabel];
        
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.textAlignment = NSTextAlignmentRight;
        _dateLabel.font =  [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [self.contentView addSubview:_dateLabel];
        
        _fromLabel = [[UILabel alloc] init];
        _fromLabel.textAlignment = NSTextAlignmentRight;
        _fromLabel.font =  [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [self.contentView addSubview:_fromLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
    
    CGSize commentsLabelSize = [self.commentLabel sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
    self.commentLabel.frame = CGRectMake(actualBounds.origin.x,
                                         actualBounds.origin.y,
                                         actualBounds.size.width,
                                         commentsLabelSize.height);
    
    CGSize fromLabelSize = [self.fromLabel sizeThatFits:actualBounds.size];
    self.fromLabel.frame = CGRectMake(actualBounds.origin.x,
                                      CGRectGetMaxY(self.commentLabel.frame) + 8.0f,
                                      actualBounds.size.width,
                                      fromLabelSize.height);
    
    CGSize dateLabelSize = [self.dateLabel sizeThatFits:actualBounds.size];
    self.dateLabel.frame = CGRectMake(actualBounds.origin.x,
                                      CGRectGetMaxY(self.fromLabel.frame) + 4.0f,
                                      actualBounds.size.width,
                                      dateLabelSize.height);
}

- (void)setItem:(Comment *)item {
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.firstLineHeadIndent = 8.0f;
    paragraphStyle.alignment = NSTextAlignmentJustified;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    self.commentLabel.attributedText = [[NSAttributedString alloc] initWithString:item.text attributes:attributes];
    self.fromLabel.text = item.fromFullName;
    self.dateLabel.text = [[CommentTableViewCell dateFormatter] stringFromDate:item.date];
}

@end
