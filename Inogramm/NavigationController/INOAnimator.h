//
//  INOAnimator.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 14/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol INOAnimator <UIViewControllerAnimatedTransitioning>

@end
