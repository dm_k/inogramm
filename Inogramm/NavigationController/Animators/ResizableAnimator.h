//
//  ResizableCellAnimator.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 14/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "INOTableViewAnimator.h"

@interface ResizableAnimator : NSObject <INOTableViewAnimator>

+ (instancetype)expandAnimator;
+ (instancetype)shrinkAnimator;

@end
