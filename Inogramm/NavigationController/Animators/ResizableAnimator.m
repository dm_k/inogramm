//
//  ResizableCellAnimator.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 14/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ResizableAnimator.h"

@interface ExpandAnimator : ResizableAnimator

@end

@interface ShrinkAnimator : ResizableAnimator

@end

@implementation ResizableAnimator

@synthesize indexPath;

+ (instancetype)expandAnimator {
    return [[ExpandAnimator alloc] init];
}

+ (instancetype)shrinkAnimator {
    return [[ShrinkAnimator alloc] init];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext { return 0.0f; }
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {}

@end

@implementation ExpandAnimator

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIView *container = [transitionContext containerView];
    
    UITableView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    NSAssert2([fromView isKindOfClass:[UITableView class]], @"%@ should animate %@ class transition", NSStringFromClass([self class]), NSStringFromClass([UITableView class]));
    
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    toView.frame = [container convertRect:[fromView rectForRowAtIndexPath:self.indexPath] fromView:fromView];
    [container addSubview:toView];
    
    NSArray<UITableViewCell *> *visibleCells = [fromView visibleCells];
    UITableViewCell *selectedCell = [fromView cellForRowAtIndexPath:self.indexPath];
    NSUInteger selectedCellVisibleIndex = [visibleCells indexOfObject:selectedCell];
    
    CGAffineTransform moveUpTransform = CGAffineTransformMakeTranslation(0.0f, - (selectedCell.frame.origin.y - fromView.contentOffset.y));
    CGAffineTransform moveDownTransform = CGAffineTransformMakeTranslation(0.0f, fromView.bounds.size.height - (selectedCell.frame.origin.y + selectedCell.frame.size.height - fromView.contentOffset.y));
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0.0f
                        options:0
                     animations:^{
                         toView.frame = fromView.frame;
                         
                         SEL setContainerBackground = NSSelectorFromString(@"setContainerBackground:");
                         if ([toView respondsToSelector:setContainerBackground]) {
                             IMP setPhotoBackgroundImp = [toView methodForSelector:setContainerBackground];
                             void (*func)(id, SEL, UIColor *) = (void *)setPhotoBackgroundImp;
                             func(toView, setContainerBackground, [UIColor clearColor]);
                         }
                         
                         [visibleCells enumerateObjectsUsingBlock:^(UITableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                             if (idx < selectedCellVisibleIndex) {
                                 obj.transform = moveUpTransform;
                             }
                             else if (idx == selectedCellVisibleIndex) {
                                 obj.hidden = YES;
                             }
                             else {
                                 obj.transform = moveDownTransform;
                             }
                         }];
                     }
                     completion:^(BOOL finished) {
                         selectedCell.hidden = NO;
                         
                         [visibleCells enumerateObjectsUsingBlock:^(UITableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                             obj.transform = CGAffineTransformIdentity;
                         }];
                         
                         [fromView removeFromSuperview];
                         [transitionContext completeTransition:finished];
                     }];
}

@end

@implementation ShrinkAnimator

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    
    UIView *container = [transitionContext containerView];
    
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    
    UITableView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    toView.frame = fromView.frame;
    [toView scrollToRowAtIndexPath:self.indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    [container insertSubview:toView belowSubview:fromView];
    
    NSAssert2([toView isKindOfClass:[UITableView class]], @"%@ should animate %@ class transition", NSStringFromClass([self class]), NSStringFromClass([UITableView class]));
    
    NSArray<UITableViewCell *> *visibleCells = [toView visibleCells];
    UITableViewCell *selectedCell = [toView cellForRowAtIndexPath:self.indexPath];
    NSUInteger selectedCellVisibleIndex = [visibleCells indexOfObject:selectedCell];
    
    CGAffineTransform moveUpTransform = CGAffineTransformMakeTranslation(0.0f, - (selectedCell.frame.origin.y - toView.contentOffset.y));
    CGAffineTransform moveDownTransform = CGAffineTransformMakeTranslation(0.0f, toView.bounds.size.height - (selectedCell.frame.origin.y + selectedCell.frame.size.height - toView.contentOffset.y));
    
    [visibleCells enumerateObjectsUsingBlock:^(UITableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx < selectedCellVisibleIndex) {
            obj.transform = moveUpTransform;
        }
        else if (idx == selectedCellVisibleIndex) {
            obj.hidden = YES;
        }
        else {
            obj.transform = moveDownTransform;
        }
    }];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0.0f
                        options:0
                     animations:^{
                         fromView.frame = [container convertRect:[toView rectForRowAtIndexPath:self.indexPath] fromView:toView];
                         
                         SEL setContainerBackground = NSSelectorFromString(@"setContainerBackground:");
                         if ([fromView respondsToSelector:setContainerBackground]) {
                             IMP setPhotoBackgroundImp = [fromView methodForSelector:setContainerBackground];
                             void (*func)(id, SEL, UIColor *) = (void *)setPhotoBackgroundImp;
                             func(fromView, setContainerBackground, [UIColor hw_colorWithHexInt:INODeYorkColorHex]);
                         }
                         
                         [visibleCells enumerateObjectsUsingBlock:^(UITableViewCell * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                             obj.transform = CGAffineTransformIdentity;
                         }];
                     }
                     completion:^(BOOL finished) {
                         selectedCell.hidden = NO;
                         [fromView removeFromSuperview];
                         
                         [transitionContext completeTransition:finished];
                     }];
}


@end
