//
//  TableViewAnimator.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 12/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "INOAnimator.h"

@protocol INOTableViewAnimator <INOAnimator>

@property (nonatomic, strong) NSIndexPath *indexPath;

@end
