//
//  PhotosTableViewCell.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "PhotosTableViewCell.h"
#import "PhotoContentView.h"

static CGFloat const PhotosTableViewCellContainerMargin = 8.0f;


@interface PhotosTableViewCell ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@property (nonatomic, strong) PhotoContentView *photoContentView;

@end

@implementation PhotosTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

+ (CGFloat)heightForItem:(Photo *)item widht:(CGFloat)widht {
    CGFloat height = [PhotoContentView heightForItem:item widht:widht - PhotosTableViewCellContainerMargin * 2.0f];
    return height + PhotosTableViewCellContainerMargin * 2.0f;
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[PhotosTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _contentInsets = UIEdgeInsetsMake(PhotosTableViewCellContainerMargin,
                                          PhotosTableViewCellContainerMargin,
                                          PhotosTableViewCellContainerMargin,
                                          PhotosTableViewCellContainerMargin);
        
        _photoContentView = [[PhotoContentView alloc] init];
        _photoContentView.layer.cornerRadius = 5.0f;
        _photoContentView.backgroundColor = [UIColor hw_colorWithHexInt:INODeYorkColorHex];

        [self.contentView addSubview:_photoContentView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.photoContentView.frame = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
}

- (void)setItem:(Photo *)item {
    [self.photoContentView setItem:item];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.photoContentView prepareForReuse];
}

@end
