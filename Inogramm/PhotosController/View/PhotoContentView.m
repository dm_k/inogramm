
//
//  PhotoContentView.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 09/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "PhotoContentView.h"
#import "Photo.h"

#import <UIImageView+WebCache.h>

static CGFloat const PhotoContentViewContentMargin = 8.0f;
static CGFloat const PhotoContentViewContentOffset = 8.0f;

@interface PhotoContentView ()

@property (nonatomic, strong) UIImageView *photoImageView;

@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *likesLabel;


@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation PhotoContentView

+ (CGFloat)heightForItem:(Photo *)item widht:(CGFloat)widht {
    CGFloat actualWidht = widht - PhotoContentViewContentMargin * 2.0f;
    
    CGFloat height = PhotoContentViewContentMargin * 2.0f + actualWidht;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:[UIFont systemFontSize]],
                                  NSParagraphStyleAttributeName : paragraphStyle};
    
    NSString *likesString = [NSString stringWithFormat:@"%@ ♡", item.likesCount];
    CGSize likesSize = [likesString boundingRectWithSize:CGSizeMake(actualWidht, CGFLOAT_MAX)
                                                 options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                              attributes:attributes
                                                 context:nil].size;
    
    CGSize detailSize = [item.detail boundingRectWithSize:CGSizeMake(actualWidht - PhotoContentViewContentOffset - likesSize.width, CGFLOAT_MAX)
                                                  options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                               attributes:attributes
                                                  context:nil].size;
    height += MAX(likesSize.height, detailSize.height) + PhotoContentViewContentOffset;
    return height;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _contentInsets = UIEdgeInsetsMake(PhotoContentViewContentMargin,
                                          PhotoContentViewContentMargin,
                                          PhotoContentViewContentMargin,
                                          PhotoContentViewContentMargin);
        
        _photoImageView = [[UIImageView alloc] init];
        _photoImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_photoImageView];
        
        _descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.numberOfLines = 0;
        _descriptionLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [self addSubview:_descriptionLabel];
        
        _likesLabel = [[UILabel alloc] init];
        _likesLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        [self addSubview:_likesLabel];
        
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _indicatorView.hidden = YES;
        [self.photoImageView addSubview:_indicatorView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
    self.photoImageView.frame = CGRectMake(actualBounds.origin.x,
                                           actualBounds.origin.y,
                                           actualBounds.size.width,
                                           actualBounds.size.width);
    
    CGSize likesSize = [self.likesLabel sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
    CGSize descriptionSize = [self.descriptionLabel sizeThatFits:CGSizeMake(actualBounds.size.width - likesSize.width - PhotoContentViewContentOffset, CGFLOAT_MAX)];
    
    self.likesLabel.frame = CGRectMake(actualBounds.origin.x + (actualBounds.size.width - likesSize.width),
                                       CGRectGetMaxY(self.photoImageView.frame) + PhotoContentViewContentOffset,
                                       likesSize.width,
                                       likesSize.height);
    
    self.descriptionLabel.frame = CGRectMake(actualBounds.origin.x,
                                             CGRectGetMaxY(self.photoImageView.frame) + PhotoContentViewContentOffset,
                                             descriptionSize.width,
                                             descriptionSize.height);
    
    self.indicatorView.center = CGPointMake(self.photoImageView.bounds.size.width / 2.0f,
                                            self.photoImageView.bounds.size.height / 2.0f);

}

- (void)setItem:(Photo *)item {
    self.descriptionLabel.text = item.detail;
    self.likesLabel.text = [NSString stringWithFormat:@"%@ ♡", item.likesCount];
    [self.photoImageView sd_setImageWithURL:[NSURL URLWithString:item.url]];
    
    [self setNeedsLayout];
}

- (void)prepareForReuse {
    self.photoImageView.image = nil;
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat actualWidht = size.width - PhotoContentViewContentMargin * 2.0f;
    CGFloat height = PhotoContentViewContentMargin * 2.0f + actualWidht;
    
    CGSize likesSize = [self.likesLabel sizeThatFits:CGSizeMake(actualWidht, CGFLOAT_MAX)];
    CGSize descriptionSize = [self.descriptionLabel sizeThatFits:CGSizeMake(actualWidht - likesSize.width - PhotoContentViewContentOffset, CGFLOAT_MAX)];
    
    return CGSizeMake(size.width, height + MAX(likesSize.height, descriptionSize.height) + PhotoContentViewContentOffset);
}


@end
