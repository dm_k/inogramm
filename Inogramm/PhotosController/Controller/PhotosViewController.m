//
//  PhotosViewController.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 05/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotosTableViewCell.h"

#import "InstagrammService.h"
#import "Photo.h"

#import "AppDelegate.h"
#import "PhotosModel.h"

#import "CommentsViewController.h"

#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>

#import "ResizableAnimator.h"

@interface PhotosViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) PhotosModel *model;

@end

@implementation PhotosViewController

- (void)loadView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.view = self.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.model = [[PhotosModel alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.navigationItem.hidesBackButton = YES;

    [self.model reloadDataWithCompletion:^{
        [self.tableView reloadData];
        [self.tableView setShowsInfiniteScrolling:[self.model canLoadNextPart]];
    }];
    
    [self.navigationController setAnimator:[ResizableAnimator expandAnimator]
                         forTransitionFrom:[self class]
                                        to:[CommentsViewController class]];
    
    [self.navigationController setAnimator:[ResizableAnimator shrinkAnimator]
                         forTransitionFrom:[CommentsViewController class]
                                        to:[self class]];
    
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf.model reloadDataWithCompletion:^{
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.pullToRefreshView stopAnimating];
            [weakSelf.tableView setShowsInfiniteScrolling:[weakSelf.model canLoadNextPart]];
        }];
    } position:SVPullToRefreshPositionTop];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf.model loadNextPartWithCompletion:^{
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
            [weakSelf.tableView setShowsInfiniteScrolling:[weakSelf.model canLoadNextPart]];
        }];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model numberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhotosTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[PhotosTableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[PhotosTableViewCell alloc] init];
    }
    [cell setItem:[self.model itemForRow:indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PhotosTableViewCell heightForItem:[self.model itemForRow:indexPath.row] widht:tableView.bounds.size.width];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.navigationController setIndexPath:indexPath forTransitionBetween:[self class] and:[CommentsViewController class]];
    
    CommentsViewController *commentsViewController = [[CommentsViewController alloc] initWithPhoto:[self.model itemForRow:indexPath.row]];
    [self.navigationController pushViewController:commentsViewController animated:YES];
}

@end
